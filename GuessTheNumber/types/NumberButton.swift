//
//  NumberButton.swift
//  GuessTheNumber
//
//  Created by TheL0w3R on 01/03/2019.
//  Copyright © 2019 TheL0w3R. All rights reserved.
//

import UIKit

@IBDesignable class NumberButton: CustomButton {
    
    @IBInspectable var numberValue: Int = 0
    
}
