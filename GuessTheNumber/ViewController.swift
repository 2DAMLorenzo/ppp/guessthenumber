//
//  ViewController.swift
//  GuessTheNumber
//
//  Created by TheL0w3R on 27/02/2019.
//  Copyright © 2019 TheL0w3R. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var tries: Int = 3
    private var winnerNumber: Int = -1
    
    @IBOutlet var numberButtons: [NumberButton]!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startNewGame()
    }
    
    @IBAction func onNumberButtonAction(_ sender: NumberButton) {
        if sender.numberValue != winnerNumber {
            sender.isEnabled = false
            tries -= 1
            consumeTry()
        } else {
            populateWin()
        }
    }
    
    @IBAction func onRestartButtonAction(_ sender: CustomButton) {
        tries = 3
        for button in numberButtons {
            button.isEnabled = true
        }
        startNewGame()
    }
    
    private func updateTriesLabel() {
        infoLabel.text = "Intentos restantes: \(tries)"
    }
    
    private func consumeTry() {
        if tries <= 0 {
            infoLabel.text = "Has perdido! El número correcto era el \(winnerNumber)!"
            for button in numberButtons {
                button.isEnabled = false
            }
            return
        }
        updateTriesLabel()
    }
    
    private func populateWin() {
        infoLabel.text = "Has acertado!!"
        for button in numberButtons {
            button.isEnabled = false
        }
    }
    
    private func startNewGame() {
        updateTriesLabel()
        winnerNumber = Int.random(in: 0...9)
    }
    
}

